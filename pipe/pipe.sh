#!/usr/bin/env bash
#
# Deploy to AWS S3, http://aws.amazon.com/s3/
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   S3_BUCKET
#   LOCAL_PATH
#
# Optional globals:
#   CONTENT_ENCODING
#   ACL
#   STORAGE_CLASS
#   CACHE_CONTROL
#   EXPIRES
#   DELETE_FLAG
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

# mandatory parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
S3_BUCKET=${S3_BUCKET:?'S3_BUCKET variable missing.'}
LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}

if [[ ! -d "${LOCAL_PATH}" ]]; then
  fail "LOCAL_PATH must be a directory."
fi

# default parameters
EXTRA_ARGS=${EXTRA_ARGS:=""}


declare -A AWS_S3_ARGS=()
AWS_S3_ARGS["cache-control"]=${CACHE_CONTROL}
AWS_S3_ARGS["content-encoding"]=${CONTENT_ENCODING}
AWS_S3_ARGS["acl"]=${ACL}
AWS_S3_ARGS["expires"]=${EXPIRES}
AWS_S3_ARGS["storage-class"]=${STORAGE_CLASS}

# Build the arguments string
ARGS_STRING=""

for key in "${!AWS_S3_ARGS[@]}"
do
  if [ -n "${AWS_S3_ARGS[$key]}" ]; then
    ARGS_STRING+="--$key=${AWS_S3_ARGS[$key]} "
  fi
done

if [[ "${DELETE_FLAG}" == "true" ]]; then
  ARGS_STRING+="--delete "
fi

ARGS_STRING+="${EXTRA_ARGS}"

info "Starting deployment to S3..."
run aws s3 sync ${LOCAL_PATH} s3://${S3_BUCKET}/ ${ARGS_STRING[@]} ${aws_debug_args}
if [[ "${status}" -eq 0 ]]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
