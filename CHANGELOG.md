# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.4

- patch: Added contribution guidelines
- patch: Updated contributing guidelines

## 0.2.3

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.2.2

- patch: Standardising README and pipes.yml.

## 0.2.1

- patch: Fix README.md typo.

## 0.2.0

- minor: Allow debugging of the aws s3 command via the DEBUG variable.
- minor: Convert from tasks to pipes.

## 0.1.3

- patch: Use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Remove details

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines S3 deploy pipe.

