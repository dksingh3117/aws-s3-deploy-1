# Bitbucket Pipelines Pipe: AWS S3 deploy

Pipe to deploy to [Amazon S3](https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html).
Syncs directories and S3 prefixes. Recursively copies new and updated files from the source local directory to the destination. Only creates folders in the destination if they contain one or more files.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: atlassian/aws-s3-deploy:0.2.4
  variables:
    AWS_ACCESS_KEY_ID: '<string>'
    AWS_SECRET_ACCESS_KEY: '<string>'
    AWS_DEFAULT_REGION: '<string>'
    S3_BUCKET: '<string>'
    LOCAL_PATH: '<string>'
    # CONTENT_ENCODING: '<string>' # Optional.
    # ACL: '<string>' # Optional.
    # STORAGE_CLASS: '<string>' # Optional.
    # CACHE_CONTROL: '<string>' # Optional.
    # EXPIRES: '<timestamp>' # Optional.
    # DELETE_FLAG: '<boolean>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*)         |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (*)     |  AWS secret key. |
| AWS_DEFAULT_REGION (*)        |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html) in the _Amazon Web Services General Reference_. |
| S3_BUCKET (*)                 |  S3 bucket name. |
| LOCAL_PATH (*)                |  Local path to folder to be deployed. |
| CONTENT_ENCODING              |  Content encodings that have been applied to the object. |
| ACL                           |  ACL for the object when the command is performed. Valid values are: `private`, `public-read`, `public-read-write`, `authenticated-read`, `bucket-owner-read`, `bucket-owner-full-control`. Default: `private`. |
| STORAGE_CLASS                 |  Type of storage to use for the object. Valid options are `STANDARD`, `REDUCED_REDUNDANCY`, `STANDARD_IA`, `ONEZONE_IA`. Default: `STANDARD`.  |
| CACHE_CONTROL                 |  Caching behavior along the request/reply chain. Valid options are `no-cache`, `no-store`, `max-age=<seconds>`, `s-maxage=<seconds> no-transform`, `public`, `private`. Default: `no-cache`. |
| EXPIRES                       |  Date and time at which the object is no longer cacheable. ISO 8601 format: `YYYY-MM-DDThh:mm:ssTZD`. Defaults to unset. |
| DELETE_FLAG                   |  Destination path is cleaned up before the upload. Default: `false`. |
| EXTRA_ARGS                    |  Extra arguments to be passed to the CLI (see AWS docs for more details). Defaults to unset. |
| DEBUG                         |  Turn on extra debug information. Default: `false`. |

_(*) = required variable._

More info about parameters and values can be found in the AWS official documentation: https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html

## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/aws-s3-deploy:0.2.4
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
```

### Advanced example: 
    
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:0.2.4
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
      ACL: 'public-read'
      CACHE_CONTROL: 'max-age=3600'
      EXPIRES: '2018-10-01T00:00:00+00:00'
      DELETE_FLAG: 'true'
      EXTRA_ARGS: '--follow-symlinks --quiet'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,aws
