#!/usr/bin/env bats
#
# Test S3
#

set -e

setup() {
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}
  RANDOM_NUMBER=$RANDOM
  LOCAL_DIR=tmp/s3-${RANDOM_NUMBER}/

  # clean up & create paths
  rm -rf $LOCAL_DIR
  mkdir -p $LOCAL_DIR

  echo "Pipelines is awesome!" > $LOCAL_DIR/deployment-${RANDOM_NUMBER}.txt
}

teardown() {
  # clean up
  rm -rf $LOCAL_DIR
}

@test "upload to S3 bucket" {
  # execute tests
  run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION="ap-southeast-2" \
      -e S3_BUCKET="bbci-task-s3-deploy-test/test" \
      -e ACL="public-read" \
      -e LOCAL_PATH=$LOCAL_DIR \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
  $IMAGE_NAME

  [[ "${status}" == "0" ]]

  # verify
  run curl --silent "https://s3-ap-southeast-2.amazonaws.com/bbci-task-s3-deploy-test/test/deployment-${RANDOM_NUMBER}.txt"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "Pipelines is awesome!" ]]
}

@test "should fail if LOCAL_PATH is not a directory" {

  touch hello.txt

  # execute tests
  run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION="ap-southeast-2" \
      -e S3_BUCKET="bbci-task-s3-deploy-test/test" \
      -e ACL="public-read" \
      -e LOCAL_PATH=hello.txt\
      -v $(pwd):$(pwd) \
      -w $(pwd) \
  $IMAGE_NAME

  [[ "${status}" != "0" ]]
  [[ "${output}" =~ "LOCAL_PATH must be a directory" ]]
}

